export const areaVariable = [
  {
    value: "Ninguno",
    label: "Sin filtro",
  },
  {
    value: "Primaria",
    label: "Primaria",
  },
  {
    value: "Secundaria 2",
    label: "Secundaria 2",
  },
  {
    value: "Secundaria 3",
    label: "Secundaria 3",
  },
  {
    value: "Secundaria 2 y 3",
    label: "Secundaria 2 y 3",
  },
  {
    value: "Balance",
    label: "Balance",
  },
  {
    value: "N/A",
    label: "N/A",
  },
  {
    value: "Stacker CV13",
    label: "Stacker CV13",
  },
  {
    value: "High Grade",
    label: "High Grade",
  },
  {
    value: "Stacker CV15",
    label: "Stacker CV15",
  },
  {
    value: "Aglomerador AD01",
    label: "Aglomerador AD01",
  },
  {
    value: "Aglomerador AD02",
    label: "Aglomerador AD02",
  },
];

export const UDM = [
  {
    value: "Ninguno",
    label: "Sin filtro",
  },
  {
    value: "Ton",
    label: "Ton",
  },
  {
    value: "Hrs",
    label: "Hrs",
  },
  {
    value: "T/H",
    label: "T/H",
  },
  {
    value: "%",
    label: "%",
  },
];

export const Desc = [
  {
    value: "Ninguno",
    label: "Sin filtro",
  },
  {
    value: "Tonelaje Humedo",
    label: "Tonelaje Humedo",
  },
  {
    value: "Tonelaje Seco",
    label: "Tonelaje Seco",
  },
  {
    value: "Horas Operadas",
    label: "Horas Operadas",
  },
  {
    value: "Productividad",
    label: "Productividad",
  },
  {
    value: "Disponibilidad",
    label: "Disponibilidad",
  },
  {
    value: "Utilización",
    label: "Utilización",
  },
]
