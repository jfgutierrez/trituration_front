import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { XAxis, YAxis, Label, ResponsiveContainer, BarChart, Bar, CartesianGrid, Tooltip, Legend } from 'recharts';
import Title from './Title';

export default function ChartFiltro({data,fecha,varia}) {
 
  
  const theme = useTheme();
  console.log('hola desde chartfiltro',data)

  

  //const {}
  
  //const fecha = datoUno.fecha;
  //console.log('hola desde char 3', fecha)
 /* const [dataFilter, setDataFilter]= useState([]);

  var dataTable=[];
  var dataPrimaria = {
    descripcion:'',
    udm:'',
    valorT1:0,
    valorT2:0,
    valorDia:0,
    valorMes:0
  }
  const obtenerDatos=(caso)=>{
    switch(caso){
      case 'PRIMARIA':
           setDataFilter(props.data.filter(f=> f.tipoDato === "HUMEDO" && f.areaVariable !=="PRIMARIA"));
           dataPrimaria.descripcion = "Tonelaje Humedo";
           dataPrimaria.udm = dataFilter.udm;
           dataPrimaria.valorT1 = dataFilter.valorT1;
           dataPrimaria.valorT2 = dataFilter.varlorT2;
           dataPrimaria.valorDia = dataFilter.totalDia;
           dataTable.push(dataPrimaria);
           console.log(dataTable);
        break;
    }
  }*/
  return (
    <React.Fragment>
      <ResponsiveContainer>
        {/*<LineChart
          data={dataTrituration.filter(dt=> dt.tipoDato === "HUMEDO")}
          margin={{
            top: 10,
            right: 10,
            bottom: 0,
            left: 20,
          }}
        >
          <XAxis dataKey="descripcionVariable" stroke={theme.palette.text.secondary} angle={45} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.secondary }}
            >
              Tonelaje
            </Label>
          </YAxis>
          <Line type="monotone" dataKey="valor" stroke={theme.palette.primary.main} dot={false} />
        </LineChart>*/}
        <BarChart
          width={400}
          height={400}
          data={data}
          margin={{
            top: 5,
            right: 15,
            left: 20,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="1 1" />
          <XAxis dataKey="decripcionReporte" angle={345} interval={0} dx={0} dy={8} fontSize={9}/>
          <YAxis stroke={theme.palette.text.secondary}>
          <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.secondary }}
            >
              {varia}
            </Label>
          </YAxis>
          <Tooltip />
          <Legend />
          <Bar dataKey="valorT1"  fill="#1A2544" barSize={35} name="Turno 1" />
          {/*<Bar dataKey="tonSecT1" fill="#D4AC0D" barSize={30} name="Ton T1 Seco" />*/}
          <Bar dataKey="valorT2"  fill="#414B72" barSize={35} name='Turno 2' />
          {/*<Bar dataKey="tonSecT2" fill="#D4AC0D" barSize={30} name="Ton T2 Seco" />*/}
          <Bar dataKey="valorDia" fill="#314C71" barSize={35} name="Total Dia"/>
          {/*<Bar dataKey="totalDiaSec" fill="#00695C" barSize={30} name="Ton día Seco"/>*/}
        </BarChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
