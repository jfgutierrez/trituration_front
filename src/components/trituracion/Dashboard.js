import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { mainListItems, secondaryListItems } from "./listItems.js";
import Chart from "./Chart.js";
import Orders from "./DataTable.js";
import axios from "axios";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  MenuItem,
  TextField,
} from "@material-ui/core";
import { areaVariable, currencies, UDM, Desc } from "./data.js";
import Title from "./Title.js";
import { useStyles } from "./style.js";
import ChartFiltro from "./ChartFiltro.js";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import SaveIcon from '@material-ui/icons/Save';
import ExportToExcel from "../Helpers/zzz.jsx";


export default function Dashboard() {
  const classes = useStyles();
  let yesterday = new Date();
  yesterday =
    yesterday.getDate() -
    1 +
    "-" +
    (yesterday.getMonth() + 1) +
    "-" +
    yesterday.getFullYear();
  const arrCopy = yesterday.split("-");
  if (parseInt(arrCopy[1]) < 10) {
    arrCopy[1] = zeroFill(arrCopy[1], 2);
  }
  if (parseInt(arrCopy[0]) < 10) {
    arrCopy[0] = zeroFill(arrCopy[0], 2);
  }
  const dateYesterday = arrCopy[2] + "-" + arrCopy[1] + "-" + arrCopy[0];
  yesterday = arrCopy[2] + arrCopy[1] + arrCopy[0];
  yesterday = yesterday.toString();

  const [open, setOpen] = React.useState(true);
  const [currency, setCurrency] = React.useState("Ninguno");
  const [dataTrituration, setDataTrituration] = useState([]);
  const [dataFilter, setDatafilter] = useState([]);
  const [unidad, setUnidad] = useState("Ninguno");
  const [fecha, setFecha] = useState(yesterday);
  const [date, setDate] = useState(dateYesterday);
  const [dataChart, setDataChart] = useState(dataTrituration);
  const [onof, setonof] = useState(true);
  const [description, setDescription] = useState("Ninguno");
  const [secondFase, setsecondFase] = useState([]);
  const [datosTabla, setDatosTabla] = useState([]);

  //ABRIR DRAWER
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  //CERRAR DRAWER
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const updateDatosChart = (data, valor) => {
    const datos = data.filter((dat) => dat.areaVariable === valor);
    setDataChart(datos);
    console.log("hola desde update datos chart", datos);
  };

  //FILTRO AREA
  const handleChange2 = (e) => {
    setCurrency(e.target.value);
    if (e.target.value === "Ninguno") {
      setDataTrituration(dataFilter);
      setUnidad("Ninguno");
      setonof(true);
      setDescription("Ninguno");
    } else if (
      e.target.value !== "Ninguno" &&
      unidad === "Ninguno" &&
      description === "Ninguno"
    ) {
      const filtro = dataFilter.filter(
        (key) => key.areaVariable === e.target.value
      );
      setDataTrituration(filtro);
      setsecondFase(filtro);
      setonof(false);
      updateDatosChart(filtro, e.target.value);
    } else if (
      (e.target.value !== "Ninguno" && unidad !== "Ninguno") ||
      description !== "Ninguno"
    ) {
      const filtro = secondFase.filter(
        (key) => key.areaVariable === e.target.value
      );
      setDataTrituration(filtro);
    } else {
      const filtro = secondFase.filter(
        (key) => key.areaVariable === e.target.value
      );
      setDataTrituration(filtro);
    }
    /*setCurrency(e.target.value);
    if ((unidad !== "Ninguno") & (e.target.value !== "Ninguno")) {
      const filtro = dataTrituration.filter(
        (key) => key.areaVariable === e.target.value
      );
      setDataTrituration(filtro);
      updateDatosChart(filtro, e.target.value);
      setonof(false);
    } else if (e.target.value === "Ninguno") {
      setDataTrituration(dataFilter);
      setUnidad("Ninguno");
      setonof(true);
      setDescription("Ninguno")
    } else {
      const filtro = dataFilter.filter(
        (key) => key.areaVariable === e.target.value
      );
      setDataTrituration(filtro);
      updateDatosChart(filtro, e.target.value);
      setonof(false);
    }*/
  };

  //FILTRO UDM
  const handleChangeFiltroUDM = (e) => {
    setUnidad(e.target.value);
    if (e.target.value === "Ninguno") {
      setDataTrituration(dataFilter);
      setCurrency("Ninguno");
      setonof(true);
      setDescription("Ninguno");
    } else if (
      e.target.value !== "Ninguno" &&
      currency === "Ninguno" &&
      description === "Ninguno"
    ) {
      const filtro = dataFilter.filter((key) => key.udm === e.target.value);
      setDataTrituration(filtro);
      setsecondFase(filtro);
      setonof(true);
    } else if (
      (e.target.value !== "Ninguno" && currency !== "Ninguno") ||
      description !== "Ninguno"
    ) {
      const filtro = secondFase.filter((key) => key.udm === e.target.value);
      setDataTrituration(filtro);
      setonof(true);
    } else {
      const filtro = secondFase.filter((key) => key.udm === e.target.value);
      setDataTrituration(filtro);
    }
    /*setUnidad(e.target.value);
    if(e.target.value!=="Ninguno"){
      const filtro = dataFilter.filter((key)=> key.udm === e.target.value)
      setDataTrituration(filtro);
    }else
    {
      setDataTrituration(dataFilter);
      setCurrency("Ninguno");
      setUnidad("Ninguno");
      setonof(true);
    };
   /* setUnidad(e.target.value);
    if ((currency !== "Ninguno") & (e.target.value !== "Ninguno")) {
      const filtro = dataTrituration.filter(
        (key) => key.udm === e.target.value
      );
      setDataTrituration(filtro);
    } else if (e.target.value === "Ninguno") {
      setDataTrituration(dataFilter);
      setCurrency("Ninguno");
      setonof(true);
      setDescription("Ninguno")
    } else {
      const filtro = dataFilter.filter((key) => key.udm === e.target.value);
      setDataTrituration(filtro);
    }*/
  };

  //FILTRO DESC
  const handleChangeDescription = (e) => {
    setDescription(e.target.value);
    if (e.target.value === "Ninguno") {
      setDataTrituration(dataFilter);
      setCurrency("Ninguno");
      setonof(true);
      setUnidad("Ninguno");
    } else if (
      e.target.value !== "Ninguno" &&
      currency === "Ninguno" &&
      unidad === "Ninguno"
    ) {
      const filtro = dataFilter.filter(
        (key) => key.decripcionReporte === e.target.value
      );
      setDataTrituration(filtro);
      setsecondFase(filtro);
      setonof(true);
    } else if (
      (e.target.value !== "Ninguno" && currency !== "Ninguno") ||
      unidad !== "Ninguno"
    ) {
      const filtro = secondFase.filter(
        (key) => key.decripcionReporte === e.target.value
      );
      setDataTrituration(filtro);
      setonof(true);
    } else {
      const filtro = secondFase.filter(
        (key) => key.decripcionReporte === e.target.value
      );
      setDataTrituration(filtro);
    }
    /*setDescription(e.target.value);
    if(e.target.value!=="Ninguno"){
      const filtro = dataFilter.filter((key)=> key.decripcionReporte === e.target.value);
      setDataTrituration(filtro);
    }else
    {
      setDataTrituration(dataFilter);
      setCurrency("Ninguno");
      setUnidad("Ninguno");
      setonof(true);
    }*/
  };

  //PARA CONVERTIR FECHA AL FORMATO QUE RECIBE DATE PICKER
  const handleChangeDatePicker = (e) => {
    setDate(e.target.value);
    const strFecha = e.target.value.toString().replace(/-/, "");
    const strFechaSin = strFecha.toString().replace(/-/, "");
    setFecha(strFechaSin);
    setCurrency("Ninguno");
    setUnidad("Ninguno");
    setDescription("Ninguno");
  };

  //PARA OBTENER LOS DATOS DEL BACK
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const url = "https://localhost:44320/api/trituration/";
  //const url = "https://localhost:44320/api/trituration/";
  const getDataByDate = async () => {
    console.log("fecha de consulta ", fecha);
    await axios
      .get(url + "getData?date=" + fecha)
      .then((response) => {
        console.log("response data", response.data);
        setDatafilter(response.data);
        updateDatosChart(response.data);
        setDataTrituration(response.data);
        arrFormatDate(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  //FUNCION PARA DARLE FORMATO A LA FECHA QUE VIENE DE LA CONSULTA
  const arrFormatDate = (arr) => {
    for (let cont = 1; cont <= arr.length; cont++) {
      const ArrString = arr[cont - 1].fecha.split("T");
      arr[cont - 1].fecha = ArrString[0];
    }
    //console.log(arr);
    //for (let cont = 1; cont <= arr.length; cont++) {
    //  const valorUno = milliFormat(arr[cont-1].valorT1)
    //  arr[cont - 1].valorT1 = valorUno
    //}

    //setDatosTabla(arr);
    // setDataTrituration(arr);
  };

  function milliFormat(num) {
    // Agregar miles
    let s = num.toString();
    if (/[^0-9\.]/.test(s)) return "invalid value";
    s = s.replace(/^(\d*)$/, "$1.");
    s = (s + "00").replace(/(\d*\.\d\d)\d*/, "$1");
    s = s.replace(".", ",");
    var re = /(\d)(\d{3},)/;
    while (re.test(s)) {
      s = s.replace(re, "$1,$2");
    }
    s = s.replace(/,(\d\d)$/, ".$1");
    return s.replace(/^\./, "0.");
  }

  function addCommas(nStr) {
    nStr += "";
    var x = nStr.split(".");
    var x1 = x[0];
    var x2 = x.length > 1 ? "." + x[1] : "";
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, "$1" + "," + "$2");
    }
    return x1 + x2;
  }

  useEffect(() => {
    getDataByDate();
    console.log("hola");
  }, [fecha]);

  function zeroFill(number, width) {
    width -= number.toString().length;
    if (width > 0) {
      return new Array(width + (/\./.test(number) ? 2 : 1)).join("0") + number;
    }
    return number + "";
  }

  const headers = [
    { label: "ID", key: "ID" },
    { label: "Área Varible", key: "Área Varible" },
    { label: "Descripcion", key: "Descripcion" },
    { label: "UDM", key: "UDM" },
    { label: "Turno 1", key: "Turno 1" },
    { label: "Turno 2", key: "Turno 2" },
    { label: "Total Dia", key: "Total Dia" },
    { label: "Fecha", key: "Fecha" },
  ];

console.log("dataTrituracion : ", dataTrituration)

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(
              classes.menuButton,
              open && classes.menuButtonHidden
            )}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.title}
          >
            Trituración
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
        <Divider />
        {/*<List>{secondaryListItems}</List>*/}
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container spacing={1}>
            <Grid item xs={12} md={6} lg={2}>
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>
                    <Title>Filtros</Title>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Grid item xs={12} md={12} lg={12}>
                    <Paper className={fixedHeightPaper}>
                      <TextField
                        className={classes.marginTopText}
                        id="date"
                        label="Fecha consulta"
                        type="date"
                        defaultValue={fecha}
                        sx={{ width: 220 }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        onChange={(e) => handleChangeDatePicker(e)}
                      />
                      <TextField
                        className={classes.marginTopText}
                        id="areavariable"
                        select
                        value={currency}
                        helperText="Por área variable"
                        onChange={(e) => handleChange2(e)}
                      >
                        {areaVariable.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                      <TextField
                        className={classes.marginTopText}
                        id="Descripcion"
                        select
                        value={description}
                        helperText="Por descripcion"
                        onChange={(e) => handleChangeDescription(e)}
                      >
                        {Desc.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                      <TextField
                        className={classes.marginTopText}
                        id="udm"
                        select
                        value={unidad}
                        helperText="Por UDM"
                        onChange={(e) => handleChangeFiltroUDM(e)}
                      >
                        {UDM.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Paper>
                  </Grid>
                </AccordionDetails>
              </Accordion>
            </Grid>
            {onof ? (
              <Grid item xs={12} md={6} lg={10}>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography>
                      <Title>Grafica - Fecha - {date}</Title>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid item xs={12} md={12} lg={12}>
                      <Paper className={fixedHeightPaper}>
                        <Chart data={dataTrituration} fecha={date} />
                      </Paper>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </Grid>
            ) : (
              <Grid item xs={12} md={6} lg={10}>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography>
                      <Title>Grafica - Fecha - {date}</Title>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid item xs={12} md={12} lg={12}>
                      <Paper className={fixedHeightPaper}>
                        <ChartFiltro
                          data={dataChart}
                          fecha={date}
                          varia={currency}
                        />
                      </Paper>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </Grid>
            )}
            <Grid item xs={12} md={12} lg={12}>
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>
                    <Title>Tabla</Title>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Grid item xs={12} md={12} lg={12}>
                  <Grid item xs={12} md={12} lg={12} style={{textAlign:"end"}}>
                    <Button
                      variant="contained"
                      color="primary"
                      size="small"
                      className={classes.button}
                      startIcon={<SaveIcon />}
                      onClick={()=>ExportToExcel(dataTrituration,fecha)}
                    >
                      Excel
                    </Button>
                  </Grid>
                    <Paper className={classes.paperTable}>
                      {<Orders data={dataTrituration} />}
                    </Paper>
                  </Grid>
                </AccordionDetails>
              </Accordion>
            </Grid>
          </Grid>
        </Container>
      </main>
    </div>
  );
}
