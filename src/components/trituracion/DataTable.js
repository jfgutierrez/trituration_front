import * as React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
    { id: 'id', label: 'ID', minWidth: 30, align:'center' },
    {
      id: 'areaVariable',
      label: 'Área Varible',
      minWidth: 50,
      align: 'left',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'nombreVariable',
      label: 'Descripcion',
      minWidth: 50,
      align: 'left',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'udm',
      label: 'UDM',
      minWidth: 50,
      align: 'center',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
        id:'Turno_1',
        label:'Turno 1',
        minWidth: 50,
        align: 'center',
        format: (value) => value.toFixed(2),
    },
    {
        id:'Turno_2',
        label:'Turno 2',
        minWidth: 50,
        align: 'center',
        format: (value) => value.toFixed(2),
    },
    {
        id:'totalDia',
        label:'Total Dia',
        minWidth: 50,
        align: 'center',
        format: (value) => value.toFixed(2),
    },
    {
        id:'fecha',
        label:'Fecha',
        minWidth: 50,
        align: 'center',
        format: (value) => value.toLocaleString(),
    },
  ];
  
  const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 670,
      marginTop: 4
    },
    marginTable:{
      padding: 2
    },
  });
  

export default function DataTable(props) {
    const classes = useStyles();

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
        setPage(0);
    };
  return (
    <div style={{ width: '100%' }}>
     <TableContainer className={classes.container}>
            <Table aria-label="sticky table">
                <TableHead>
                    <TableRow>
                    {columns.map((column) => (
                        <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                        >
                        <strong>{column.label}</strong>
                        </TableCell>
                    ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        props.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((d)=>(
                            <TableRow key = {d.id}>
                                <TableCell align ="center">{d.id}</TableCell>
                                <TableCell align ="left">{d.areaVariable}</TableCell>
                                <TableCell left ="center">{d.decripcionReporte}</TableCell>
                                <TableCell align ="center">{d.udm}</TableCell>
                                <TableCell align ="center">{d.valorT1}</TableCell>
                                <TableCell align ="center">{d.valorT2}</TableCell>
                                <TableCell align ="center">{d.valorDia}</TableCell>
                                <TableCell align ="center">{d.fecha}</TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
        <TablePagination labelRowsPerPage="Filas por pagina"
            rowsPerPageOptions={[30, 45, 60]}
            component="div"
            count={props.data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
        />
    </div>
  );
}