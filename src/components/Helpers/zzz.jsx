import React from "react";
import ExcelJs from "exceljs";

const ExportToExcel = (data, fecha) => {
  console.log("entra al archivo helper,", data);

  let sheetName = "ReporteTrituracion.xlsx";
  let headerName = "RequestsList";

  let workbook = new ExcelJs.Workbook();
  let sheet = workbook.addWorksheet(sheetName, {
    views: [{ showGridLines: false }],
  });
  //HEADER
  let columnArr = [];
  for (let i in data[0]) {
    let tempObj = { name: "" };
    tempObj.name = i;
    columnArr.push(tempObj);
  }

  sheet.addTable({
    name: `Header`,
    ref: "D2",
    headerRow: true,
    totalsRow: false,
    style: {
      theme: "",
      showRowStripes: false,
      showFirstColumn: true,
      width: 200,
    },
    columns: [{ name: "Reporte Trituracion" }],
    rows: [["Fecha"], [fecha.fecha]],
  });

  sheet.addTable({
    name: headerName,
    ref: "A5",
    headerRow: true,
    totalsRow: false,
    style: {
      theme: "TableStyleMedium2",
      showRowStripes: false,
      width: 200,
    },
    columns: columnArr ? columnArr : [{ name: "" }],
    rows: data.map((e) => {
      let arr = [];
      for (let i in e) {
        arr.push(e[i]);
      }
      return arr;
    }),
  });

  sheet.getCell("D2").font = { size: 18, bold: true };

  sheet.columns = sheet.columns.map((e) => {
    const expr = e.values[5];
    switch (expr) {
      case "id":
        return { width: 5 };
      case "idVariable":
        return { width: 10 };
      case "descripcionVariable":
        return { width: 45 };
      case "decripcionReporte":
        return { width: 18 };
      case "areaVariable":
        return { width: 18 };
      case "tipoDato":
        return { width: 19 };
      case "udm":
        return { width: 8 };
      case "valorT1":
        return { width: 10 };
      case "valorT2":
        return { width: 10 };
      case "valorDia":
        return { width: 10 };
      case "fecha":
        return { width: 12 };
      default:
        return { width: 50 };
    }
  });

  const table = sheet.getTable(headerName);
  for (let i = 0; i < table.table.columns.length; i++) {

    sheet.getCell(`${String.fromCharCode(65 + i)}5`).font = { size: 12 };
    sheet.getCell(`${String.fromCharCode(65 + i)}5`).fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: "c5d9f1" },
    };

    for (let j = 0; j < table.table.rows.length; j++) {
      let rowCell = sheet.getCell(`${String.fromCharCode(65 + i)}${j + 6}`);
      rowCell.alignment = { wrapText: true };
      rowCell.border = {
        bottom: {
          style: "thin",
          color: { argb: "a6a6a6" },
        },
      };
    }
  }
  table.commit();

  const writeFile = (fileName, content) => {
    const link = document.createElement("a");
    const blob = new Blob([content], {
      type: "application/vnd.ms-excel;charset=utf-8;",
    });
    link.download = fileName;
    link.href = URL.createObjectURL(blob);
    link.click();
  };

  workbook.xlsx.writeBuffer().then((buffer) => {
    writeFile(sheetName, buffer);
  });
};

export default ExportToExcel;
