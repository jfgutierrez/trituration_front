import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Trituracion from './components/trituracion/Dashboard.js';


function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Trituracion}  />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
